# timeout-titan

This is a Discord bot, that allows moderators to timeout a user by specifying the amount of minutes, this can be anywhere from 1 minute to almost a month.

But it also features Monero integration and per default allows users to timeout other Discord users from the server for 0.01 XMR per hour